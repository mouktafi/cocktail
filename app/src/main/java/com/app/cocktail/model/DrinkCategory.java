package com.app.cocktail.model;

public class DrinkCategory {

    private String categoryName;

    public DrinkCategory(String categoryName) {
        this.categoryName = categoryName;
    }
    public DrinkCategory() {
    }


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
