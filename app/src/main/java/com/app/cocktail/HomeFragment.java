package com.app.cocktail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.app.cocktail.adapter.DrinksAdapter;
import com.app.cocktail.model.DrinkCategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class HomeFragment extends Fragment {

    ProgressDialog progressDialog;
    ArrayList<DrinkCategory> drinkCategories;
    DrinksAdapter drinksAdapter;
    ListView listView;
    String catName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createDialog();
        DownloadTask task = new DownloadTask();
        task.execute("https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list");

        drinkCategories = new ArrayList<DrinkCategory>();

        listView = requireActivity().findViewById(R.id.lv_list_frag_home);

        drinksAdapter = new DrinksAdapter(getActivity(),R.layout.item_drink_cat,drinkCategories);
        listView.setAdapter(drinksAdapter);
    }

    public void createDialog(){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Attendi");
        progressDialog.setMessage("Caricamento lista ingredienti");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void closeDialog(){
        progressDialog.dismiss();
    }

    public class DownloadTask extends AsyncTask<String,Void, String> {

        @Override
        protected String doInBackground(String... strings) {

            StringBuilder result = new StringBuilder();
            URL url;
            HttpURLConnection urlConnection;

            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();
                while (data != -1) {
                    char cur = (char)data;
                    result.append(cur);
                    data = reader.read();
                }
                return result.toString();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.i("JSON", result);

            try {
                JSONObject jsonObject = new JSONObject(result);

                String drinksCats_str = jsonObject.getString("drinks");
                JSONArray array = new JSONArray(drinksCats_str);

                for (int i=0; i<array.length(); i++) {
                    DrinkCategory dc = new DrinkCategory();
                    JSONObject jsonPart = array.getJSONObject(i);
                    catName = jsonPart.getString("strIngredient1");

                    dc.setCategoryName(catName);
                    drinkCategories.add(dc);
                    Log.i("DC", catName);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        JSONObject catX;
                        String catDef;
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            try {
                                catX = array.getJSONObject(position);
                                catDef = catX.getString("strIngredient1");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.i("toastred", catDef);
                            //Log.i("toastred", String.valueOf(catX));
                            Intent i = new Intent(getActivity(), DetailActivity.class);
                            i.putExtra("ingredient", catDef);
                            startActivity(i);
                        }
                    });

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            drinksAdapter.notifyDataSetChanged();
            closeDialog();
        }
    }
}