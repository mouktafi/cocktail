package com.app.cocktail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import com.app.cocktail.adapter.DrinksAdapter;
import com.app.cocktail.model.DrinkCategory;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.cocktail.databinding.ActivityDetailBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    private ActivityDetailBinding binding;

    Bundle bundle;
    ArrayList<DrinkCategory> drinkCategories;
    DrinksAdapter drinksAdapter;
    String catName;
    TextView tvIngDesc;
    AppBarLayout appBarLayout;
    ProgressDialog progressDialog;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        bundle = getIntent().getExtras();
        String ingredientName = bundle.getString("ingredient");

        tvIngDesc = findViewById(R.id.tv_ing_desc);

        appBarLayout = findViewById(R.id.app_bar);
        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolBarLayout = binding.toolbarLayout;
        toolBarLayout.setTitle(ingredientName);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        //appBarLayout.setBackground(Drawable.createFromPath("https://www.thecocktaildb.com/images/ingredients/gin.png"));
        //appBarLayout.setBackgroundColor(Color.parseColor("#00ff00"));
        //appBarLayout.setBackground(Drawable.createFromPath("@drawable/side_nav_bar"));
        //appBarLayout.setBackgroundResource(R.drawable.ic_baseline_wine_bar_24);

        url = "https://www.thecocktaildb.com/images/ingredients/" + ingredientName + ".png";
        new FetchImage(url).start();

        FloatingActionButton fab = binding.fab;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        createDialog();

        DwldIngDet task = new DwldIngDet();
        task.execute("https://www.thecocktaildb.com/api/json/v1/1/search.php?i=" + ingredientName);

        drinkCategories = new ArrayList<DrinkCategory>();
        drinksAdapter = new DrinksAdapter(getApplicationContext(),R.layout.item_drink_cat,drinkCategories);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void createDialog(){
        progressDialog = new ProgressDialog(this);


        progressDialog.setTitle("Attendi");
        progressDialog.setMessage("Caricamento Dettagli");
        progressDialog.setCancelable(false);

        progressDialog.show();
    }
    public void closeDialog(){
        progressDialog.dismiss();
    }

    class FetchImage extends Thread {

        String URL;
        Bitmap bitmap;


        FetchImage(String URL) {
            this.URL = URL;
        }
        @Override
        public void run() {

            InputStream inputStream;
            try {
                inputStream = new URL(URL).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Drawable d = new BitmapDrawable(getResources(), bitmap);
            appBarLayout.setBackground(d);
        }

    }

    public class DwldIngDet extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... strings) {

            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();
                while (data != -1) {
                    char cur = (char)data;
                    result += cur;
                    data = reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = new JSONObject(s);

                String drinksCats_str = jsonObject.getString("ingredients");
                JSONArray array = new JSONArray(drinksCats_str);

                for (int i=0; i<array.length(); i++) {
                    DrinkCategory dc = new DrinkCategory();
                    JSONObject jsonPart = array.getJSONObject(i);
                    catName = jsonPart.getString("strDescription");

                    dc.setCategoryName(catName);
                    drinkCategories.add(dc);
                    Log.i("DC", catName);

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            tvIngDesc.setText(catName);
            drinksAdapter.notifyDataSetChanged();
            closeDialog();
        }
    }
}