package com.app.cocktail.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.cocktail.R;
import com.app.cocktail.model.DrinkCategory;

import java.util.ArrayList;
import java.util.List;



public class DrinksAdapter extends ArrayAdapter<DrinkCategory> {

    private ArrayList<DrinkCategory> drinkCats;
    private LayoutInflater layoutInflater;
    private int Resource;
    Context context;

    public DrinksAdapter(Context context, int resource, ArrayList<DrinkCategory> objects) {
        super(context, resource, objects);

        this.context = context;
        this.Resource = resource;
        this.drinkCats = objects;
        layoutInflater = (LayoutInflater.from(context));
    }

    private static class ViewHolder{
        TextView catName;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(Resource,null);
            viewHolder = new ViewHolder();
            viewHolder.catName = convertView.findViewById(R.id.tv_cat_name);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.catName.setText(drinkCats.get(position).getCategoryName());
        return convertView;

    }
}
